{
  # testing 2.7.1 update
  pkgs ? import (fetchTarball {
    url = "https://github.com/marius851000/nixpkgs/archive/802da448f15e362098d9fcf7fdce5297f0057747.tar.gz";
    sha256 = "sha256:0c3r6m7vi7jmf2aakfqc4vz87lb42h3vchrcvpx44y1f5qgc5h5k";
  }) {},
  openmw-mods ? <openmw-mods>,
  meta ? <meta>,
  python ? <python>,
  CACHE ? "/portbuild/cache/",
  MORROWIND_PATH ? "/portbuild/morrowind",
  mode ? "mod"
}:

let
  to_build = import ./default.nix {
    inherit pkgs mode;
    portmod = pkgs.portmod.overrideAttrs (old: {
      postPatch = (if old ? postPatch then old.postPatch else "") + ''
        substituteInPlace portmod/execute.py \
          --replace "/gnu" "/portbuild\", \"/nexusback"
        substituteInPlace portmod/merge.py \
          --replace "protect = not env.INTERACTIVE and not env.TESTING" "protect = False"
      '';
      disabledTests = old.disabledTests ++ [
        "test_cfg_protect_license"
        "test_cfg_protect_keywords"
        "test_cfg_protect_use"
      ];
    });
    portmod_config = {
      inherit CACHE MORROWIND_PATH;
      EXTRA_MARKER = "none";
      PREFIX = "morrowind";
      PREFIX_TYPE = "openmw";
      PROFILE_KEY = "5";
      REPOS = {
        openmw = {
          path = openmw-mods;
        };
        meta = {
          path = meta;
          add_to_profile = false;
        };
        python = {
          path = python;
        };
      };
    };
  };
in
  pkgs.lib.mapAttrs' (name: value: pkgs.lib.nameValuePair (pkgs.lib.replaceStrings ["/"] ["|"] name) value) to_build