from portmodlib.atom import Atom
from portmod.globals import env
from portmod.query import query
from portmodlib.usestr import use_reduce
from portmod.loader import load_pkg
from portmod._deps import resolve
from portmod.transactions import Delete
from portmod.download import download_mod
from portmodlib.source import HashAlg
from portmod.config.sets import get_set

import sys
import json

def source_manifest_to_source_definition(source_manifest):
    hashes = {}
    for hashalg in source_manifest.hashes:
        hashes[str(hashalg).split("HashAlg.")[-1].lower()] = source_manifest.hashes[hashalg]
    return {
        "url": source_manifest.url,
        "name": source_manifest.name,
        "basename": source_manifest.basename,
        "hashes": hashes
    }

IGNORED_PACKAGES = ["common/placeholder"]

env.set_prefix(sys.argv[1])

commons = []
for package in query("*", 0):
    if package.cpn.startswith("common") and package.cpn not in IGNORED_PACKAGES:
        commons.append(package.cpn)

base_packages = get_set("world")
base_transactions = resolve(
    base_packages,
    set(),
    base_packages,
    base_packages,
    set(),
    emptytree = True
)
base_sources = []
for trans in base_transactions.pkgs:
    if not isinstance(trans, Delete):
        for source in trans.pkg.get_source_manifests(trans.flags):
            base_sources.append(source_manifest_to_source_definition(source))

packages = {}
package_count = 0
for package in query("*", 0):
    if package.cpn in IGNORED_PACKAGES:
        continue
    all_versions = load_pkg(Atom(package.cpn))

    for pybuild in all_versions:
        if "::installed" in pybuild.__str__():
            continue
        pybuild_key = pybuild.__str__().split("::")[0]
        deps = use_reduce(pybuild.RDEPEND)
        deps.extend(use_reduce(pybuild.DEPEND))
        
        version_set = {Atom(pybuild_key)}
        print(pybuild_key)
        new_deps = []
        all_sources_with_deps = []
        try:
            transactions = resolve(
                version_set,
                set(),
                version_set,
                version_set,
                set(),
                emptytree = True
            )
            print("ok...")
            
            for trans in transactions.pkgs:
                if not isinstance(trans, Delete):
                    for source in trans.pkg.get_source_manifests(trans.flags):
                        all_sources_with_deps.append(source_manifest_to_source_definition(source))
        except KeyboardInterrupt:
            raise
        except BaseException as e:
            print(e)
        
        all_sources = {}
        for source in pybuild.get_source_manifests(matchall = True):
            all_sources[source.basename] = source_manifest_to_source_definition(source)

        packages[pybuild_key] = {
            "all_sources": all_sources,
            "all_sources_with_deps": all_sources_with_deps
        }
        package_count += 1

print("{} packages will be built".format(package_count))
f = open(sys.argv[2], "w")
json.dump({
    "packages": packages,
    "common": commons,
    "base_sources": base_sources
}, f)
f.close()