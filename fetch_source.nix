{ stdenv, lib, python3, wget, local_cache, busybox }:

rec {
  # can’t use mkDerivation, it refuse md5
  fetch_portmod_source = info: let
    
  in
    derivation {
      system = stdenv.system;
      name = "source-" + (lib.strings.sanitizeDerivationName info.name);
      builder = "${python3}/bin/python3";
      args = [ ./fetch_source.py ];
      BASENAME = info.basename;
      NAME = info.name;
      URL = info.url;
      WGET_PATH = "${wget}/bin/wget";
      CACHE = local_cache;
      CP_PATH = "${busybox}/bin/cp";

      outputHashMode = "flat";
      outputHashAlgo = "md5";
      outputHash = info.hashes."md5";
    };
  
  fetch_all_sources = sources_info:
    stdenv.mkDerivation {
      name = "portmod-sources-group";

      dontUnpack = true;

      installPhase = ''
        mkdir $out
        ${lib.concatLines (
          builtins.map (x:
            "ln -s ${lib.escapeShellArg (fetch_portmod_source x)} $out/${lib.escapeShellArg (x.name)}"
          ) sources_info
        )}
      '';
    };
}