{ python3, base_dir, derivation_tool, lib }:

let
  data_file = derivation_tool.mkPortmodUserDerivation base_dir {
    name = "parse_portmod_packages.json";

    nativeBuildInputs = [ python3 ];

    buildPhase = ''
      python3 ${./parse_data.py} $PORTMOD_PREFIX ./parsed.json
    '';

    installPhase = "cp parsed.json $out";
  };
in
  lib.importJSON data_file