{ pkgs ? import <nixpkgs> {}, portmod ? pkgs.portmod, portmod_config, mode ? "mod" }:

# if mode is "mod", will only build mods, if mode is "source", will only build sources

let
  derivation_tool = pkgs.callPackage ./derivation_tool.nix {
    inherit portmod portmod_config;
  };

  base_dir = derivation_tool.mkNewPortmodProfile;

  parsed_data = pkgs.callPackage ./parse_data.nix {
    inherit derivation_tool base_dir;
  };

  base_dir_bis = derivation_tool.mkPortmodUserDerivation base_dir {
    name = "portmod-common-packages";
    buildPhase = ''
      portmod $PORTMOD_PREFIX merge --update --deep @world --no-confirm

      mkdir $out
      touch $out/sucess
    '';

    save = true;

    cache = fetch_source.fetch_all_sources parsed_data.base_sources;
  };

  fetch_source = pkgs.callPackage ./fetch_source.nix {
    local_cache = portmod_config.CACHE;
  };

  packages = (pkgs.lib.mapAttrs (cpn: package_data: {
    build = (if mode == "mod" then (derivation_tool.mkPortmodUserDerivation base_dir_bis {
        name = "portmod-check-${pkgs.lib.strings.sanitizeDerivationName cpn}" ;
        buildPhase = ''
          portmod $PORTMOD_PREFIX merge ${cpn} --no-confirm
          
          mkdir $out
          touch $out/sucess
        '';
        cache = fetch_source.fetch_all_sources package_data.all_sources_with_deps;
      }) else {});
    } // (if mode == "source" then (pkgs.lib.mapAttrs (basename: info:
        fetch_source.fetch_portmod_source info
      )
      package_data.all_sources
    ) else {})
  ) parsed_data.packages);
in
  packages