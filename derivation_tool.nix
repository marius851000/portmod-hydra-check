{ stdenv, lib, portmod, python3, hexdump, portmod_config, writeText }:

let
  # Can’t just link to the nix store, as they are read-only
  repos_cfg_base = lib.generators.toINI { } (
      lib.mapAttrs (k: v:
        {
          location = "/homeplaceholder/${k}";
          auto_sync = false;
        }
      ) portmod_config.REPOS
    );

  repos_cfg_file = writeText "repos.cfg" repos_cfg_base;

  setup_repos_script = ''
    mkdir -p $HOME/.config/portmod
    cp ${repos_cfg_file} $HOME/.config/portmod/repos.cfg
    substituteInPlace $HOME/.config/portmod/repos.cfg \
      --replace false False \
      --replace /homeplaceholder $HOME/repos
  '';
in {
  mkPortmodUserDerivation =
    let
      python_correct_paths_file_text = ''
        import sys
        import os
        import subprocess

        # fix repos
        f = open(os.path.join(sys.argv[1], ".config/portmod/repos.cfg"))

        r = ""
        for line in f.readlines():
          if ".local/share/portmod" in line:
            r += "location = {}/.local/share/portmod/{}".format(sys.argv[1], line.split(".local/share/portmod/")[-1])
          else:
            r += line
        f.close()

        f = open(os.path.join(sys.argv[1], ".config/portmod/repos.cfg"), "w")
        f.write(r)
        f.close()

        # fix profile symlink
        symlink_to_fix_path = os.path.join(sys.argv[1], ".config/portmod/morrowind/profile")
        temp = os.readlink(symlink_to_fix_path).split(".local/share/")[-1]
        new_target = os.path.join(sys.argv[1], ".local/share", temp)
        subprocess.check_call(["rm", symlink_to_fix_path])
        subprocess.check_call(["ln", "-s", new_target, symlink_to_fix_path])
      '';
      python_correct_paths_file = builtins.toFile "correct_repo_path.py" python_correct_paths_file_text;
    in
    base_user: x: let
      save = if x ? save then x.save else false;
    in stdenv.mkDerivation (x // {
      nativeBuildInputs = (if x ? nativeBuildInputs then x.nativeBuildInputs else []) ++ [ portmod python3 hexdump ];

      unpackPhase = ''

        echo marker: ${portmod_config.EXTRA_MARKER}
        cp -r --reflink=auto ${base_user}/home ./home
        export HOME=$PWD/home/
        export PORTMOD_PREFIX=${portmod_config.PREFIX}
        chmod -R +w $HOME
        python3 ${python_correct_paths_file} $HOME
        rm -fr ~/.cache/portmod/downloads
        ${if (x ? cache) then
          "ln -s ${x.cache} ~/.cache/portmod/downloads"
        else
          "mkdir ~/.cache/portmod/downloads"
        }
        echo "displaying cache"
        ls -l ~/.cache/portmod/downloads/*
        echo "done displaying cache"
        export MORROWIND_PATH=${portmod_config.MORROWIND_PATH}
        ${setup_repos_script}
      '';

      postInstall = (if save then ''
        mv home $out/home
      '' else "");
    }
  );

  mkBaseUserDir = 
    source_dir: stdenv.mkDerivation {
    name = "base-user-dir";

    nativeBuildInputs = [ ];

    dontUnpack = true;

    installPhase = ''
      mkdir -p $out/
      cp -r --reflink=auto ${source_dir} $out/home
      echo marker: ${portmod_config.EXTRA_MARKER}
    '';
  };

  mkNewPortmodProfile = stdenv.mkDerivation {
    name = "new-home-dir";

    nativeBuildInputs = [ portmod ];

    dontUnpack = true;

    buildPhase = ''
      echo marker: ${portmod_config.EXTRA_MARKER}

      export HOME=$PWD/home
      export PORTMOD_PREFIX=${portmod_config.PREFIX}

      ${setup_repos_script}

      cat $HOME/.config/portmod/repos.cfg
      
      mkdir -p ~/repos
      ${lib.concatLines (
        (lib.mapAttrsToList (k: v:
          "cp -r --reflink=auto ${v.path} ~/repos/${k}"
        ) portmod_config.REPOS)
      )}

      chmod -R u+w ~/repos

      portmod sync

      portmod init $PORTMOD_PREFIX ${portmod_config.PREFIX_TYPE} --no-confirm

      ${lib.concatLines (
        (lib.mapAttrsToList (k: v:
          if (if v ? add_to_profile then v.add_to_profile else true) then
            "portmod $PORTMOD_PREFIX select repo add ${k}"
          else
            ""
        ) portmod_config.REPOS)
      )}

      portmod $PORTMOD_PREFIX select profile set ${portmod_config.PROFILE_KEY}
    '';

    installPhase = ''
      mkdir $out
      mv home $out/home
    '';
  };
}