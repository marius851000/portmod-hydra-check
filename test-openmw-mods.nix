{
  # latest master as of 12-11-2023 (dd-mm-yyyy)
  pkgs ? import (fetchTarball {
    url = "https://github.com/marius851000/nixpkgs/archive/802da448f15e362098d9fcf7fdce5297f0057747.tar.gz";
    sha256 = "sha256:0c3r6m7vi7jmf2aakfqc4vz87lb42h3vchrcvpx44y1f5qgc5h5k";
  }) {},
  openmw-mods ? (builtins.fetchGit {
    url = "https://gitlab.com/portmod/openmw-mods.git";
    rev = "3ab60d0dc20b5814d9caac554a9887f972e44508";
  }),
  meta ? (builtins.fetchGit {
    url = "https://gitlab.com/portmod/meta.git";
    rev = "c38df0c0376116472b5abb3a2db2a0c4d2f79702";
  }),
  python ? (builtins.fetchGit {
    url = "https://gitlab.com/portmod/python.git";
    rev = "d79d54b3a435ebe7a23e0bf0045900fb0897eb5d";
  }),
}:

let
  to_build = import ./hydra_jobs.nix {
    inherit openmw-mods meta python;
    CACHE = "/tmp/portmod-download/";
    MORROWIND_PATH = "/tmp/morrowind";
  };

  depOnAll = x: let
    command = pkgs.lib.concatLines
      (pkgs.lib.mapAttrsToList (
        k: v: "echo ${v.name}:${v} >> out.txt"
      ) x);
  in pkgs.stdenv.mkDerivation {
    name = "portmod-result";
    dontUnpack = true;
    buildPhase = command;
    installPhase = "cp out.txt $out";
  };
in
  depOnAll to_build."base|example-suite-0.17"