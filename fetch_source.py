import os
import subprocess

url = os.environ['URL']
basename = os.environ['BASENAME']
name = os.environ['NAME']
wget_path = os.environ['WGET_PATH']
cache = os.environ['CACHE']
out = os.environ['out']
cp_path = os.environ['CP_PATH']

sucess = False
if url != "" and url != basename:
    print("fetching {}".format(url))
    try:
        subprocess.check_call([wget_path, url, "-O", out])
        sucess = True
    except BaseException as e:
        print(e)

if not sucess:
    mirror_url = "https://gitlab.com/portmod-mirrors/openmw/-/raw/master/" + name
    print("fetching {}".format(mirror_url))
    try:
        subprocess.check_call([wget_path, mirror_url, "-O", out])
        sucess = True
    except BaseException as e:
        print(e)

if not sucess:
    local_path = os.path.join(cache, name)
    print("fetching from the local cache ({})".format(local_path))
    try:
        subprocess.check_call([cp_path, local_path, out])
        sucess = True
    except BaseException as e:
        print(e)

if not sucess:
    print("fetch failed for {}!".format(name))
else:
    print("fetch sucess for {}.".format(name))